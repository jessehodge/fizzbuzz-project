from rest_framework import status
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet

from fizz.models import FizzBuzz
from fizz.serializers import FizzBuzzSerializer


class FizzBuzzViewSet(ModelViewSet):
    """
    This is the FizzBuzz ViewSet

    It performs all CRUD functionality for FizzBuzz objects
    ~ create
    ~ update
    ~ retrieve
    ~ list
    ~ delete
    Via the ModelViewSet mixins
    """
    queryset = FizzBuzz.objects.all()
    serializer_class = FizzBuzzSerializer

    def create(self, request, *args, **kwargs):
        user_agent = request.headers.get('User-Agent')
        data = request.data
        request.POST._mutable = True
        data['useragent'] = user_agent
        request.POST._mutable = False
        serializer = self.get_serializer(data=data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)
