from django.db import models


# Create your models here.
class FizzBuzz(models.Model):
    """
    Our FizzBuzz model contains the following fields

    fizzbuzz_id: The primary key of the FizzBuzz Table
    useragent: The User-Agent used for the request that was made
    creation_date: The creation date of the FizzBuzz object
    message: The random message that was sent in the request
    """
    fizzbuzz_id = models.BigAutoField(primary_key=True)
    useragent = models.CharField(blank=True, max_length=200, null=True)
    creation_date = models.DateTimeField(auto_now=True)
    message = models.CharField(max_length=200)
