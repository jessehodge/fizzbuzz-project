from rest_framework.serializers import ModelSerializer, DateTimeField, IntegerField, CharField
from fizz.models import FizzBuzz


class FizzBuzzSerializer(ModelSerializer):
    """
    This is the FizzBuzzSerializer that validates our data and
    gives us the fields that are mutable and ready_only
    """
    creation_date = DateTimeField(read_only=True)
    fizzbuzz_id = IntegerField(read_only=True)

    class Meta:
        model = FizzBuzz
        fields = [
            'creation_date',
            'message',
            'fizzbuzz_id',
            'useragent'
        ]
        extra_kwargs = {
            'useragent': {
                'required': False,
                'allow_blank': True,
                'allow_null': True
            }
        }
