from rest_framework.routers import DefaultRouter
from fizz.views import FizzBuzzViewSet

router = DefaultRouter()
router.register('fizzbuzz', FizzBuzzViewSet)
urlpatterns = router.urls
