from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase

from fizz.models import FizzBuzz


class TestFizzBuzzViews(APITestCase):

    def test_create_fizzbuzz(self):
        """
        Ensure that we can create FizzBuzz objects
        :return: A single FizzBuzz object
        """
        url = reverse('fizzbuzz-list')
        headers = {
            'User-Agent': "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko"
                          ") Chrome/95.0.4638.69 Safari/537.36"
        }
        data = {"message": "This is a test message", "useragent": ""}
        response = self.client.post(url, data, format='json', headers=headers)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(FizzBuzz.objects.count(), 1)
        self.assertEqual(FizzBuzz.objects.get().message, "This is a test message")

    def test_get_fizzbuzz(self):
        """
        Creates a FizzBuzz object
        Test retrieving a single FizzBuzz object
        :return: {
            'creation_date': '2021-11-24T03:24:27.041520Z',
            'message': 'This is a test message',
            'fizzbuzz_id': 1,
            'useragent': None
        }
        """
        post_url = reverse('fizzbuzz-list')
        headers = {
            'User-Agent': "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko"
                          ") Chrome/95.0.4638.69 Safari/537.36"
        }
        data = {"message": "This is a test message", "useragent": ""}
        self.client.post(post_url, data, format='json', headers=headers)
        url = reverse('fizzbuzz-detail', args=[1])
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(FizzBuzz.objects.count(), 1)
        self.assertEqual(FizzBuzz.objects.get().message, "This is a test message")

    def test_get_fizzbuzz_list(self):
        """
        Create 10 FizzBuzz objects
        Retrieve all 10 objects in a single Get request
        """
        for __ in range(10):
            post_url = reverse('fizzbuzz-list')
            headers = {
                'User-Agent': "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko"
                              ") Chrome/95.0.4638.69 Safari/537.36"
            }
            data = {"message": "This is a test message", "useragent": ""}
            self.client.post(post_url, data, format='json', headers=headers)
        url = reverse('fizzbuzz-list')
        list_response = self.client.get(url)
        self.assertEqual(list_response.status_code, status.HTTP_200_OK)
        self.assertEqual(FizzBuzz.objects.count(), 10)