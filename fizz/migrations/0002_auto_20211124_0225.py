# Generated by Django 3.2.9 on 2021-11-24 02:25

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('fizz', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='fizzbuzz',
            name='id',
        ),
        migrations.AddField(
            model_name='fizzbuzz',
            name='fizzbuzz',
            field=models.IntegerField(default=1, primary_key=True, serialize=False),
            preserve_default=False,
        ),
    ]
