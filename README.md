# Instructions to run locally

1. Create a local_settings.py with a new SECRET_KEY in it.
2. Create a virtual environment with VirtualEnv
3. Activate your virtualenv
4. Run `pip install -r requirements.txt` in order to install all relevant libraries
